# ign-cmake2-release

The ign-cmake2-release repository has moved to: https://github.com/ignition-release/ign-cmake2-release

Until May 31st 2020, the mercurial repository can be found at: https://bitbucket.org/osrf-migrated/ign-cmake2-release
